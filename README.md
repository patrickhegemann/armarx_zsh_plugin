# ArmarX Workspace plugin for zsh (oh-my-zsh)

## Installation


## Manual installation
1. Copy `plugins/armarx/armarx.plugin.zsh` to `$ZSH/custom/plugins/armarx`
2. Copy `themes/bira_custom.zsh-theme` to `$ZSH/custom/themes` or customize your own theme. For this you have to modify the `$PROMPT` variable and add the `$ZSH_THEME_ARMARX_PREFIX` and `$ZSH_THEME_ARMARX_SUFFIX` variables to your theme file, as shown in the provided example.
3. Load the plugin in your .zshrc (`plugins=(... armarx)`) and set the theme accordingly (`ZSH_THEME="bira_custom"` or your own theme)

