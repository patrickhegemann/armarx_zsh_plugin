function armarx_prompt_info(){
  [[ -n ${ARMARX_WORKSPACE} ]] || return
  echo "${ZSH_THEME_ARMARX_PREFIX=[}${ARMARX_WORKSPACE:t}${ZSH_THEME_ARMARX_SUFFIX=]}"
}

